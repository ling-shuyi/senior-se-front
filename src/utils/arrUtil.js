export default {
    moveElementForward(arr, i) {
        if (i > 0 && i < arr.length) {
            var temp = arr[i];
            arr[i] = arr[i - 1];
            arr[i - 1] = temp;
        }
    },
    moveElementBackward(arr, i) {
        if (i >= 0 && i < arr.length - 1) {
            const elementToMove = arr.splice(i, 1)[0]; // 移除第i个元素并获取它的值
            arr.splice(i + 1, 0, elementToMove); // 将移除的元素插入到数组中的下一个位置
        }
    }
}