import {openReturn} from "@/utils/pages/pageUtil";

export default {
    user(){
        return {
            str(){
                return '/user'
            },
            open(){
                return openReturn(this.str())
            }
        }
    }
}