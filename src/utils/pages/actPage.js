import {openReturn, queryStr} from "@/utils/pages/pageUtil";
import {decode} from "urlencode";

export default {
    info(actId){
        return {
            str(){
                return '/act/info'+queryStr({actId})
            },
            open(){
                return openReturn(this.str())
            }
        }
    },
    create(){
        return {
            str(){
                return '/act/edit'
            },
            open(){
                return openReturn(this.str())
            }
        }
    },
    
}