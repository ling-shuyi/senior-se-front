import qs from "qs";

const openReturn = (href) => {
    return {
        self(){
            window.open(href, '_self')
        },
        blank(){
            window.open(href, '_blank')
        },
        replace(){
            // 修改浏览器历史记录，将C页面添加到历史记录中，上一级历史是B页面
            history.pushState({}, '', href);
            // 跳转到C页面
            window.location.href = href;
        }
    }
}

const queryStr = (data)=>{
    return "?"+qs.stringify(data)
}

export {openReturn,queryStr}