import {openReturn, queryStr} from "@/utils/pages/pageUtil";

export default {
    status(code){
        return {
            str(){
                return `/auth/status`+queryStr({code})
            },
            open(){
                return openReturn(this.str())
            }
        }
    }
}