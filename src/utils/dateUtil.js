export default {
    getDateArray(date){
        return [date.getFullYear(), date.getMonth()+1, date.getDate()]
    },
    getDateByArray(arr){
        return new Date(arr[0], arr[1]-1, arr[2])
    },
    cmpDateStr(s1, s2){
        let d1 = new Date(s1)
        let d2 = new Date(s2)
        if(d1 < d2){
            return -1
        }else if(d1 > d2){
            return 1
        }else {
            return 0
        }
    },
    cmpCurrentDateStr(s1, s2){
        let d1 = new Date(s1)
        let d2 = new Date(s2)
        let d = new Date()
        if(d1 < d2){
            if(d < d1){
                return -1
            }else if(d < d2){
                return 0
            }else {
                return 1
            }
        }
    }
}