import DBConst from "@/consts/DBConst";

export default {
    copyToClipboard(text) {
        // 创建一个<input>元素
        let input = document.createElement('input');
        // 将要复制的文本设置为<input>元素的值
        input.value = text;
        // 将<input>元素添加到文档中
        document.body.appendChild(input);
        // 选择<input>元素中的文本
        input.select();
        // 执行复制命令
        document.execCommand('copy');
        // 从文档中移除<input>元素
        document.body.removeChild(input);
    }
}