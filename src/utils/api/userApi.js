import axiosUtil from "@/utils/api/axiosUtil";

export default {
    async getUserId(){
        let res = await axiosUtil.post("/api/must/user/id")
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async getUserInfo() {
        let res = await axiosUtil.post("/api/must/user/info")
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async needUserInfo() {
        let res = await axiosUtil.post("/api/need/user/info")
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async listAct(type, sort, desc, page) {
        let data = {type, sort, desc, page}
        let res = await axiosUtil.post("/api/must/user/act/list", data)
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    }
}