import axios from "axios";
import {showConfirmDialog, showDialog} from 'vant';
import 'vant/es/dialog/style';
import wxApi from "@/utils/api/wxApi";
import authPage from "@/utils/pages/authPage";

const commonResponseHandler = (res)=>{
    console.log('url: '+res.request.responseURL);
    console.log(res);
    try {
        if(res.data.code < 0){
            switch (res.data.code){
                case -1: {
                    authPage.status(-1).open().self()
                    break
                }
                case -3: {
                    authPage.status(-3).open().replace()
                    break
                }
                case -4: {
                    authPage.status(-4).open().replace()
                    break
                }
            }
        }
    }catch (e){}
    
}

const loginDialog = function (){
    showConfirmDialog({
        title: '授权登录',
        message:
            '检测到您尚未登陆，请通过微信授权登录后方可使用网站功能',
    })
        .then(() => {
            wxApi.openAuthCode()
        })
        .catch(() => {
            // on cancel
            authPage.status(-1).open().self()
        });
}

axios.interceptors.response.use(
    res => {
        commonResponseHandler(res)
        return res;
    }, err=>{
        let status = err.response.status
        switch (status){
            case 403:
                //未登录状态
                loginDialog()
                //authPage.login().open().self()
                break
            default:
                return err
        }
    })


export {commonResponseHandler, loginDialog}
export default axios