import axiosUtil  from "@/utils/api/axiosUtil";
import qs from "qs";
import {showFailToast} from "vant";

export default {
    async loginByCode(code){
        let res = await axiosUtil.post('/api/no/auth/login/code',qs.stringify({code}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1: {
                showFailToast('登陆失败，请稍后重试')
                break
            }
        }
        throw res.data
    },
    async checkLogin(){
        let res = await axiosUtil.post('/api/need/auth/login/check')
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async loginTest(userId){
        let res = await axiosUtil.post('/api/no/auth/login/test',qs.stringify({userId}))
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async getPublicKey(){
        let res = await axiosUtil.post('/api/must/auth/key')
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    }
}