

import qs from "qs";
import {encode} from "qs/lib/utils";
import wxConst from "@/consts/wxConst";

const wxApi = {
    openAuthCode(from){
        if(!from){
            from = location.href
        }
        let data = {
            appid: wxConst.OPEN.WX_APPID,
            redirect_uri: 'http://lingshuyi.xyz/auth/login/code',  //qs自动urlencode了
            response_type: 'code',
            scope: 'snsapi_userinfo',
            state: from,
        }
        window.open(wxConst.OPEN.WX_AUTH_URL+'?'+qs.stringify(data)+'#wechat_redirect','_self')
    }
}

export default wxApi