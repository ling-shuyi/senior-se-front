import axiosUtil from "@/utils/api/axiosUtil";
import qs from "qs";
import {showFailToast} from "vant";
import authPage from "@/utils/pages/authPage";
import DBConst from "@/consts/DBConst";
import {decode} from "urlencode";
import {openReturn, queryStr} from "@/utils/pages/pageUtil";

function handleQnAns(qn) {
    qn.forEach(item => {
        if (item.ans !== null) {
            switch (item.type) {
                case DBConst.QType.SINGLE: {
                    item.ans = Number(item.ans)
                    break
                }
                case DBConst.QType.MULTI: {
                    item.ans = item.ans.split(',').map(item => {
                        return Number(item)
                    })
                    break
                }
            }
        }else if(item.type === DBConst.QType.MULTI){
            item.ans = []
        }
    })
}

function handleRecordAns({q, records}){
    
    let oMap = {}
    
    switch (q.type){
        case DBConst.QType.SINGLE:{
            q.options.forEach(item=>{
                oMap[item.oId] = item.info
            })
            
            records.forEach(item=>{
                let ans= Number(item.ans)
                item.ans = oMap[ans]
            })
            
            break
        }
        case DBConst.QType.MULTI: {
            q.ans = []
            q.options.forEach(item=>{
                oMap[item.oId] = item.info
            })
            records.forEach(item=>{
                let ans = item.ans.split(',').map(item => {
                    return Number(item)
                })
                item.ans = ans.map(item=>{
                    return oMap[item]
                }).join(';')
            })
            break
        }
    }
}

export default {
    async search(key, catId, sort, desc, page){
        let data = {key, catId, sort, desc, page}
        let res = await axiosUtil.post("/api/must/act/search", data)
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async createAct(act,files){
        const formData = new FormData();
        formData.append('act', new Blob([JSON.stringify(act)], { type: 'application/json' }));
        files.forEach(item=>{
            if(!item.imgId){
                formData.append("files", item)
            }
        })
        let res = await axiosUtil.post("/api/must/act/create", formData)
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async updateAct(act, files){
        const formData = new FormData();
        formData.append('act', new Blob([JSON.stringify(act)], { type: 'application/json' }));
        files.forEach(item=>{
            if(!item.imgId){
                formData.append("files", item)
            }
        })
        let res = await axiosUtil.post("/api/must/act/update", formData)
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1:{
                showFailToast('活动发布中不可修改')
                break
            }
            case 2: {
                showFailToast('已有人报名活动，不可修改')
                break
            }
        }
        throw res.data
    },
    async publishAct(actId){

        let res = await axiosUtil.post("/api/must/act/publish", qs.stringify({actId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1:{
                showFailToast('活动已经开始或结束，不可发布')
                break
            }
            
        }
        throw res.data
    },
    async cancelPublishAct(actId){

        let res = await axiosUtil.post("/api/must/act/publish/cancel", qs.stringify({actId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1:{
                showFailToast('活动已经开始或结束，不可取消')
                break
            }
            
        }
        throw res.data
    },
    async deleteAct(actId){

        let res = await axiosUtil.post("/api/must/act/delete", qs.stringify({actId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1:{
                showFailToast('活动处于发布状态，不可删除')
                break
            }
            
        }
        throw res.data
    },
    async getActInfo(actId){

        let res = await axiosUtil.post("/api/must/act/info", qs.stringify({actId}))
        if(res.data.ok){
            handleQnAns(res.data.data.qn);
            return res.data.data
        }
        switch (res.data.code){
            case 1: {
                authPage.status(-3).open().replace()
            }
        }
        throw res.data
    },
    async getSimpleActInfo(actId){
        
        let res = await axiosUtil.post("/api/must/act/info/simple", qs.stringify({actId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1: {
                authPage.status(-3).open().replace()
                break
            }
        }
        throw res.data
    },
    async getActQn(actId){

        let res = await axiosUtil.post("/api/must/act/qn", qs.stringify({actId}))
        if(res.data.ok){
            handleQnAns(res.data.data)
            return res.data.data
        }
        switch (res.data.code){
            case 1:
                showFailToast("问卷创建者不可报名")
                break
            case 2:
                showFailToast("报名人数已达上限")
                break
            case 3:
                showFailToast("不在报名时间内")
                break
            case 4:
                showFailToast("请勿重复报名")
                break
            case 5:
                showFailToast("活动尚未发布")
                break
        }
        throw res.data
    },
    async applyAct(actId, qn){
        let data = {actId, qn}
        let res = await axiosUtil.post("/api/must/act/apply", data)
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1:
                showFailToast("问卷创建者不可报名")
                break
            case 2:
                showFailToast("报名人数已达上限")
                break
            case 3:
                showFailToast("不在报名时间内")
                break
            case 4:
                showFailToast("请勿重复报名")
                break
            case 5:
                showFailToast("活动尚未发布")
                break
            case 6:
                showFailToast("问卷参数错误")
                break
        }
        throw res.data
    },
    async cancelApplyAct(applyId){

        let res = await axiosUtil.post("/api/must/act/apply/cancel", qs.stringify({applyId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1: {
                showFailToast("不在取消报名的时间内")
            }
        }
        throw res.data
    },
    async signQrcode(actId){

        let res = await axiosUtil.post("/api/must/act/sign/qrcode", qs.stringify({actId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1: {
                showFailToast("活动未发布")
            }
        }
        throw res.data
    },
    async signAct(actId){

        let res = await axiosUtil.post("/api/must/act/sign", qs.stringify({actId}))
        if(res.data.ok){
            return res.data.data
        }
        switch (res.data.code){
            case 1: {
                showFailToast("活动未发布")
                break
            }
            case 2: {
                showFailToast('您尚未报名活动')
                break
            }
        }
        throw res.data
    },
    async listApply(actId, page){
        let data = {actId, page}
        let res = await axiosUtil.post("/api/must/act/apply/list", data)
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    },
    async getApplyInfo(actId, applyId){

        let res = await axiosUtil.post("/api/must/act/apply/info", qs.stringify({actId,applyId}))
        if(res.data.ok){
            handleQnAns(res.data.data.qn)
            return res.data.data
        }
        throw res.data
    },
    async listApplyQ(actId, qId){
        let res = await axiosUtil.post("/api/must/act/apply/q", qs.stringify({actId,qId}))
        if(res.data.ok){
            handleRecordAns(res.data.data)
            return res.data.data
        }
        throw res.data
    },
    excel(data, userId){
        let href = `/api/no/act/apply/excel`+queryStr({data, userId})
        window.open(href, '_blank')
    }
}