import axiosUtil from "@/utils/api/axiosUtil";

export default {
    async listCat(){
        let res = await axiosUtil.post("/api/no/cat/list")
        if(res.data.ok){
            return res.data.data
        }
        throw res.data
    }
}