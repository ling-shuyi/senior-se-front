import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '/act/edit',
    name: 'editAct',
    component: () => import('./views/EditAct.vue'),
    meta: {
      title: '活动编辑'
    }
  },
  {
    path: '/act/info',
    name: 'actInfo',
    component: () => import('./views/ActInfo.vue'),
    meta: {
      title: '活动详情'
    }
  },
  {
    path: '/act/sign',
    name: 'signAct',
    component: () => import('./views/Sign.vue'),
    meta: {
      title: '活动签到'
    }
  },
  {
    path: '/act/apply',
    component: () => import('./views/ApplySta.vue'),
    meta: {
      title: '活动统计'
    },
    children: [
      {
        path: '',
        redirect: 'apply/user'
      },
      {
        path: 'user',
        name: 'applyUser',
        component: () => import('./views/ApplyUser.vue'),
        meta: {
          title: '活动报名统计'
        }
      },
      {
        path: 'info',
        name: 'applyInfo',
        component: () => import('./views/ApplyInfo.vue'),
        meta: {
          title: '活动报名详情'
        }
      },
      {
        path: 'q',
        name: 'applyQ',
        component: () => import('./views/ApplyQ.vue'),
        meta: {
          title: '活动题目统计'
        }
      }
    ]
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
