import 'vant/lib/index.css';
import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import '@/assets/style/vantStyle'

const app = createApp(App)
app.use(router)
app.mount('#app')