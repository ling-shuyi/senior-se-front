import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '/auth/login/test',
    name: 'loginTest',
    component: () => import('./views/LoginTest.vue'),
    meta: {
      title: '登陆测试'
    }
    
  },
  {
    path: '/auth/login/code',
    name: "loginCode",
    component: () => import('./views/LoginCode.vue'),
    meta: {
      title: '授权登录'
    }
  },
  {
    path: '/auth/status',
    name: 'status',
    component: () => import('./views/Status.vue'),
    meta: {
      title: '出错啦'
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router
