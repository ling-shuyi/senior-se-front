import App from './App.vue'
import {createApp} from "vue";
import router from "@/views/user/router";
import '@/assets/style/vantStyle'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.mount('#app')