import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '/user',
    name: 'user',
    component: () => import('./views/UserIndex.vue'),
    meta: {
      title: '个人中心'
    }
  },
  {
    path: '/user/act',
    name: 'userAct',
    component: () => import('./views/UserAct.vue'),
    meta: {
      title: '我的活动'
    }
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router
