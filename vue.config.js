const { defineConfig } = require('@vue/cli-service')
const { VantResolver } = require('@vant/auto-import-resolver');
const ComponentsPlugin = require('unplugin-vue-components/webpack');
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    plugins: [
      ComponentsPlugin({
        resolvers: [VantResolver()],
      }),
    ],
  },
  devServer: {
    client: {
      overlay: false
    },
    port: 4090,
    proxy: {
      '/api': {
        //target: 'http://localhost:8090/',
        target: 'http://1.94.110.14:8090/',
        changeOrigin: true,
        ws: true
      }
    }
  },
  pages: {
    index: {
      entry: 'src/views/index/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: '首页'
    },
    auth: {
      entry: 'src/views/auth/main.js',
      template: 'public/auth.html',
      filename: 'auth.html',
      title: '授权登录'
    },
    user: {
      entry: 'src/views/user/main.js',
      template: 'public/user.html',
      filename: 'user.html',
      title: '个人中心'
    },
    act: {
      entry: 'src/views/act/main.js',
      template: 'public/act.html',
      filename: 'act.html',
      title: '活动'
    }
  },

})
