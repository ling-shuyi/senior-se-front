import { createRouter, createWebHistory } from 'vue-router'
import HomeView from "@/views/index/views/HomeView";


const routes = [
  {
    path: '/auth/login/test',
    name: 'loginTest',
    component: () => import('./auth/LoginTest.vue')
  },
  {
    path: '/act/info',
    name: 'Info',
    component: () => import('./act/Info.vue')
  },
  {
    path: '/act/apply',
    name: 'apply',
    component: () => import('./act/apply.vue')
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('./user/user.vue')
  },
  {
    path: '/user/act',
    name: 'userAct',
    component: () => import('./user/userAct.vue')
  },
  {
    path: '/',
    name: 'home',
    component: () => import('./index/views/HomeView.vue')
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('./index/views/HomeView.vue')
  },
  {
    path: '/act/edit',
    name: 'edit',
    component: () => import('./act/edit.vue')
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
