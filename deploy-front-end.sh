#!/bin/sh
date
project=senior-se-front

cd /data/project/${project}

echo "拉取代码"
git pull

echo "安装依赖"
npm install

echo "项目打包"
npm run build

echo "项目打包成功"

echo "项目部署"
nginx_home=/data/usr/local/nginx
rm -rf ${nginx_home}/html/*
mv dist/* ${nginx_home}/html